<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Carbon\Carbon;

class Steps extends Model  // implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;


    public function getCreatedAtAttribute($date)
    {
        // return $date;
        // 'l d. M'

        $date = Carbon::parse($date)->setTimezone('Europe/Berlin')->locale('de');
        return $date->getTranslatedDayName() . ' ' . $date->format('d.') . ' ' . $date->getTranslatedMonthName();
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'steps',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//    protected $hidden = [
//        'password',
//    ];
}
