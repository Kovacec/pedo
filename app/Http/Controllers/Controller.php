<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @var int
     */
    protected $statusCode = 200;

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return ApiController
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondNotFound($message = 'Not Found!')
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    /**
     * @param string $message
     * @param array  $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondParametersNotValid($message = 'Not valid.', $errors = [])
    {
        return $this->setStatusCode(422)->respondWithError([
            'message' => $message,
            'errors'  => $errors
        ]);
    }

    /**
     * @param $data
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    public function respond($data, $headers = [])
    {
        return response()->json($data, $this->statusCode, $headers);
    }


    /**
     * @param       $message
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithError($data)
    {
        return $this->setStatusCode(503)->respond($data);
    }


}
