<?php

namespace App\Http\Controllers;

use App\Steps;
use Illuminate\Http\Request;

class StepsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        return Steps::latest()->get();
    }


    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'name' => 'required',
            'steps' => 'required|integer'
        ]);

        $validated['name'] = strtoupper($validated['name']);

        // check if there is already an entry for today ?

        $steps = new Steps($validated);
        $steps->save();

        return $steps;
    }

    public function destroy($id)
    {
        $steps = Steps::find($id);

        if ($steps)
        {
            $steps->delete();
            $this->respond(['Status' => 1]);
        }

        $this->respondNotFound('Steps with ID: ' .$id. ' not found');
    }

}
